<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('ChangeAvatar')):
class ChangeAvatar
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }
    public static function handle(){
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) && $_GET['custom'] == "changeAvatar") {
            global $wpdb;
            $data = json_decode(file_get_contents('php://input'), 1);
            $file = $_FILES['user_avatar']["tmp_name"];
            $user_id = sanitize_text_field($_POST['user_id']);
            $a_id = sanitize_text_field($_POST['a_id']);
            $name =$_FILES['user_avatar']["name"];
            $plugin_dir = ABSPATH . 'wp-content/plugins/Art/';
            $target_dir = $plugin_dir . '/Profiles/';
            $data = array(
                'user_id' => $user_id,
                'profile' => $name,
                'created' => date("Y-m-d H:i:s"),
            );
            move_uploaded_file($file, $target_dir . $name);
            if($a_id){
                $table = $wpdb->prefix . 'artistprofile_itg';
                $success = $wpdb->update($table, $data, array('id'=>$a_id));
            }else{
                $table = $wpdb->prefix . 'artistprofile_itg';
                $success = $wpdb->insert($table, $data);
            }
            if ($success) {
                echo json_encode(array('status' => true,'msg' => 'Uploaded Successfully'));
                die();
            } else {
                echo json_encode(array('status' => false));
                die();
            }
        }
    }
}
new ChangeAvatar();
endif;