<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('RegisterArtist')):
class RegisterArtist
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['custom']) && $_GET['custom'] == "registerArtist"){
            $data = json_decode(file_get_contents('php://input'), 1);
            require_once(ABSPATH . WPINC . '/registration.php');
            $new_user_id = array(
                'user_login'		=> sanitize_text_field($data['user_login']),
                'user_pass'	 		=> sanitize_text_field($data['user_pass']),
                'user_email'		=> sanitize_text_field($data['user_email']),
                'first_name'		=> sanitize_text_field($data['first_name']),
                'last_name'			=> sanitize_text_field($data['last_name']),
                'user_registered'	=> date('Y-m-d H:i:s'),
                'role'				=> 'artist'
            );

            $exists_email = email_exists( $new_user_id['user_email'] );
            $exists_username = username_exists(  $new_user_id['user_login'] );

            if ( $exists_email || $exists_username) {
                $response = [
                    "status"=>false,
                    "msg"=>"Duplicate entry",
                ];
                echo json_encode($response);
                die();
            }else{
                $user_id = wp_insert_user( $new_user_id ) ;
                wp_new_user_notification($user_id);
                $subject = 'Login Details';
                $to = $new_user_id['user_email'];
                $message = ' <p> Hi There! Welcome To Bellymonk. 
                            We just wanted to let you know your user account on 
                            Bellymonk.com as Artist has been approved.</p><br>
                            <span>Username:'.$new_user_id['user_login'].'</span>
                            <br> <span>'.site_url().'</span>';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: BellyMonk < bellymonk@example.com >');
                wp_mail( $to, $subject, $message, $headers );
                $response = [
                    "status"=>true,
                    "msg"=>" User Added Successfully",
                ];
                echo json_encode($response);
                die();
            }
            exit();
        }
    }

    public static function wc_custom_user_redirect($redirect, $user){
        $user_id = $user->ID;
        $role = $user->roles[0];
        $dashboard = admin_url();
        $author_url = site_url() .'?author='.$user_id ;
        $myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );

        if( $role == 'administrator' ) {
            $redirect = $dashboard;
        } elseif ( $role == 'artist' ) {
            $redirect = $author_url;
        } elseif ( $role == 'customer' || $role == 'subscriber' ) {
            $redirect = $myaccount;
        } else {
            $redirect = wp_get_referer() ? wp_get_referer() : home_url();
        }
        return $redirect;
    }

}
new RegisterArtist();
endif;