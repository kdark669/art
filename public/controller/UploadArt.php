<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('UploadArt')):
class UploadArt
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle(){
            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) && $_GET['custom'] == "uploadArt" && !isset($_GET['art_edit'])) {
                $data = json_decode(file_get_contents('php://input'), 1);
                $file = $_FILES['artist_file'];
                $category = $_POST['cat_Ids'];
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $name = sanitize_text_field($_POST['title']);
                $user_id = sanitize_text_field($data['artist_id']);

                $filename = basename($file);
                $upload_file = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));

//                $post_id = YOUR_POST_ID; //set post id to which you need to set post thumbnail
                $filename = $upload_file['file'];
                $wp_filetype = wp_check_filetype($filename, null );
                $attachment = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => sanitize_file_name($filename),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $user_id = sanitize_text_field($data['artist_id']);

                $attach_id = wp_insert_attachment( $attachment, $filename );
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );

                $image_url = $upload_file['url'];
                $user_email = get_userdata($user_id)->user_email;
                if (!$upload_file['error']) {
                    $wp_filetype = wp_check_filetype($filename, null);
                    $post = array(
                        'post_author' => $_POST['artist_id'] != ''? $_POST['artist_id'] : get_current_user_id(),
                        'post_mime_type' => $wp_filetype['type'],
                        'post_title' => $name,
                        'post_content' => '<img class="alignnone size-medium wp-image-30" src="' . $image_url . '" alt="" width="300" height="175" />',
//                        'post_status' => 'publish',
                        'post_type' => 'art'
                    );
                    $post_id = wp_insert_post($post, '');
                    update_post_meta( $post_id, '_acceptedFlag',  0 );
                    set_post_thumbnail( $post_id, $attach_id );
                    if(isset($post_id)){
                        foreach ($category as $cat){
                            $data = array(
                                'post_id' => $post_id,
                                'product_id' =>    $cat,
                            );
                            $wpdb->insert($table, $data);
                        }
                    }
//                    $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $post_id );
//                    if (!is_wp_error($attachment_id)) {
//                        //if attachment post was successfully created, insert it as a thumbnail to the post $post_id
//                        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
//                        //wp_generate_attachment_metadata( $attachment_id, $file ); for images
//                        $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
//                        wp_update_attachment_metadata( $attachment_id,  $attachment_data );
//                        set_post_thumbnail( $post_id, $attachment_id );
//                    }
                    echo json_encode(array('status' => true,'msg' => 'Uploaded Successfully'));
                    die();
//
                }
            }
            if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) && $_GET['custom'] == "uploadArt" && isset($_GET['art_edit'])){
                $data = json_decode(file_get_contents('php://input'), 1);

                $category = $_POST['cat_Ids'];
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $name = sanitize_text_field($_POST['title']);

                if(!empty($_FILES['artist_file'])) {
                    $file = $_FILES['artist_file'];
                    $filename = basename($file);
                    $upload_file = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));

//                $post_id = YOUR_POST_ID; //set post id to which you need to set post thumbnail
                    $filename = $upload_file['file'];
                    $wp_filetype = wp_check_filetype($filename, null);
                    $attachment = array(

                        'post_mime_type' => $wp_filetype['type'],
                        'post_title' => sanitize_file_name($filename),
                        'post_content' => '',
                        'post_status' => 'inherit'
                    );
                    $attach_id = wp_insert_attachment($attachment, $filename);
                    require_once(ABSPATH . 'wp-admin/includes/image.php');
                    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                    wp_update_attachment_metadata($attach_id, $attach_data);

                    $image_url = $upload_file['url'];
                    set_post_thumbnail($_GET['art_edit'], $attach_id);

                }


                $user_email = get_userdata($user_id)->user_email;
                if (!$upload_file['error']) {
                    $wp_filetype = wp_check_filetype($filename, null);
                    $post = array(
                        'ID' => $_GET['art_edit'],
                        'post_author' => $_POST['artist_id'] != ''? $_POST['artist_id'] : get_current_user_id(),
                        'post_mime_type' => $wp_filetype['type'],
                        'post_title' => $name,
                        'post_type' => 'art'
                    );
                    if(isset($image_url)) {
//                        die();
                        $post = array_merge($post, ['post_content' => '<img class="alignnone size-medium wp-image-30" src="' . $image_url . '" alt="" width="300" height="175" />']);
                    }
                    $post_id = wp_update_post($post, '');
                    update_post_meta( $post_id, '_acceptedFlag',  0 );
                    set_post_thumbnail( $post_id, $attach_id );
                    if(isset($post_id) && !empty($category)){
                        $result = $wpdb->delete($table, ['post_id' =>$post_id]);
//                        print_r($category);
                        foreach ($category as $cat){
//                            print_r($cat);
                            $data = array(
                                'post_id' => $post_id,
                                'product_id' =>    $cat,
                            );
                            $wpdb->insert($table, $data);
                        }
                    }
                    echo json_encode(array('status' => true,'msg' => 'Uploaded Successfully'));
                    die();
            }}
    }
}

new UploadArt();
endif;