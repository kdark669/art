<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('ArtistInfo')):
    class ArtistInfo
    {
        public static function our_artist_info(){
            global $product;
            $product_id = $product->get_id();
            $postmeta = get_post_meta($product_id);
            $user_id = $postmeta['_selectartist'][0];
            if(!empty($user_id)){
                $user = get_user_by( 'id', $user_id );
                $role = $user->roles[0];
                if($role == 'artist'){
                    ?>
                    <div class="large-3 columns artprofilelink">
                        <span class="some-talent">
                         <span>Art By: </span>

                 <a href="<?php echo site_url() .'?author='.$user_id?>">
                    <?php echo $user->display_name ?>
                </a>
                </span>
                    </div>
                    <?php
                }
            }


        }

        public static function add_login_Link(){
            $myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );
            if(!is_user_logged_in()) {
                ?>
                <a href="<?php echo $myaccount .'?new='.'artist'?>">
                    Become An Artist
                </a>
                <?php
            }
        }
    }
endif;