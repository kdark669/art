let edit = false;

function readURL(input) {
    console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById('blah').setAttribute('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function changeFormToEdit(el, id, title,content) {
    let sibling = el.parentElement.nextElementSibling.nextElementSibling;
    let uploadBox = document.querySelector('#uploadBox');
    if(sibling && uploadBox){
        uploadBox.classList.remove('d-none');
//     uploadBox.querySelector('input').value = sibling.alt
        document.querySelector('#bm-gallery').innerHTML = `<img src='${sibling.src}'/> `
    }
    edit = true;
    console.log(title);
    let form = document.getElementById('uploadArt');
    form['title'].value = title;
//     let doc = new DOMParser().parseFromString(content, 'text/html');
//     console.log(form);
//     doc.body.childNodes[0].id = 'blah';
//     form.childNodes[7].replaceChild(doc.body.childNodes[0],form.childNodes[7].childNodes[1]);
//     let newEle = document.createElement('DIV');
//     newEle.innerHTML = '<h2>Edit the Art :'+title+'</h2>';
//     form.insertBefore(newEle,form.childNodes[0]);
    form.id = 'editArt';
    axios.get(`/wp-json/wl/vl/posts`, {
        params: {
            'post_id': id
        }
    }).then((res) => {
        let productId = res.data;
        productId.forEach(ele => {
            let checkboxes = document.getElementById('cat' + ele.product_id);

            if (checkboxes) {
                checkboxes.checked = true
            }

        });
    });
    form = document.getElementById('editArt');
    // console.log(form.removeChild(form.childNodes[10]));
    form.addEventListener('submit', function (e) {
        console.log(e);
        e.preventDefault();
        let activeFlag = true;
        let dataToPass = getData(e);
        let url = window.location.href + '';
        axios.post(url, dataToPass, {
            headers: {
                'content-type': 'multipart/form-data' // do not forget this
            },
            params: {
                'custom': 'uploadArt',
                'art_edit': id
            }
        })
            .then(res => {
                if (res.data.status) {
                    form.id = 'uploadArt';
                    e.target['fileElem'].value = '';
                    alert('succesfully inserted post !! Please wait For administrative validation !.');

                    location.reload();
                    e.target['title'].value = '';
                    e.target['fileElem'].value = [];
                    // e.target['title'].value = '';
                } else {
                }
            })
            .catch(error => {
                console.log(error);
            })
    });

}

function validateImage(e) {
    console.log('Hello');
    var image = document.getElementById('fileElem').value;
    var eartist_file = document.getElementById('eartist_file');
    eartist_file.style.display = 'none';
    activeFlag = true;
    readURL(e.target);
}

// document.addEventListener('DOMContentLoaded', function (e) {
let uploadForm = document.getElementById('uploadArt');
uploadForm.addEventListener('submit', (e) => {

    e.preventDefault();
    let activeFlag = true;
    if (!edit) {
        if (!e.target['fileElem'].files[0]) {
            activeFlag = false;
        }
        let data_toPass = getData(e);
        let url = window.location.href + '';
        if (activeFlag) {
            axios.post(url, data_toPass, {
                headers: {
                    'content-type': 'multipart/form-data' // do not forget this
                },
                params: {
                    'custom': 'uploadArt'
                }
            })
                .then(res => {
                    if (res.data.status) {
                        e.target['fileElem'].value = '';
                        msg = res.data.msg;
                        // var success = document.getElementById('uploaded');
                        // success.innerHTML = msg;
                        // success.style.right = 0;
                        // success.style.marginRight = '5px';
                        alert('succesfully inserted post !! Please wait For administrative validation !.');
                        location.reload();
                        setTimeout(function () {
                            var success = document.getElementById('uploaded');
                            success.style.display = 'none';
                        }, 2000);
                        e.target['title'].value = '';
                        e.target['fileElem'].value = [];
                        // e.target['title'].value = '';
                    } else {
                        console.log(res.data.msg);
                        // msg = res.data.msg;
                        // var error = document.getElementById('errorR');
                        // error.innerHTML = msg;
                        // error.style.right = 0;
                        // error.style.marginRight = '5px';
                        setTimeout(function () {
                            var error = document.getElementById('errorR');
                            error.style.display = 'none';
                        }, 2000);
                    }
                })
                .catch(error => {
                    console.log(error);
                    // var errorR = document.getElementById('errorR');
                    // errorR.innerHTML = '<span>Something went wrong</span>';
                    // errorR.style.right = 0;
                    // errorR.style.marginRight = '5px';
                    setTimeout(function () {
                        var errorR = document.getElementById('errorR');
                        errorR.style.display = 'none';
                    }, 2000);
                })
        }
    }
})
// });

function getData(e){
    let data = {};
    data = new FormData();
    data.append('artist_file', e.target['fileElem'].files[0]);
    data.append('title', e.target['title'].value);
    let inputs = e.target['cat'];

    if (inputs.checked) {
        data.append('cat_Ids[]', inputs.value);
    }
    let catIDs = [];
    for (var i = 0; i < inputs.length; i++) {
        console.log(inputs[i], inputs[i].checked);
        if (inputs[i].checked) {

            // only record the checked inputs
            data.append('cat_Ids[]', inputs[i].value);
            inputs[i].checked = false;

        }
    }

    data.append('artist_id', e.target['artistId'].value);
    return data;
}