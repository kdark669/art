// lightbox

let displayImage=(view='', mode='none')=>{
	let sibling;
	if(view!==''){
		sibling=view.parentElement.nextElementSibling;
	}
	document.querySelector('.image-light-box').style.display=mode;
	document.querySelector('.light-box-image').src=sibling.src;
	document.querySelector('.light-box-title').innerHTML=sibling.alt;
	console.log(sibling.src);

}


// let editArt=el=>{
// 	let sibling=el.parentElement.nextElementSibling;
// 	let uploadBox=	document.querySelector('#uploadBox');
// 	uploadBox.classList.remove('d-none');
// 	uploadBox.querySelector('input').value=sibling.alt
// 	document.querySelector('#bm-gallery').innerHTML=`<img src='${sibling.src}'/> `
// }
// image-upload

let changeImage=(file)=>{
	document.querySelector('#'+file).click();
}

let displayUploadBox=(mode=true)=>{
	if(!mode){
		let form2 = document.getElementById('editArt');
		console.log(form2);
		if(form2){
		form2.id='uploadArt';
		}
		document.querySelector('#uploadBox').classList.add('d-none');
	}else{
		document.querySelector('#uploadBox').classList.remove('d-none');
		let form1 = document.getElementById('editArt');
		form1['title'] = '';
	}
	document.querySelector('#bm-gallery').innerHTML=` `
}

let dropArea = document.getElementById("drop-area");
['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, preventDefaults, false)
	document.body.addEventListener(eventName, preventDefaults, false)
})

// Highlight drop area when item is dragged over it
;['dragenter', 'dragover'].forEach(eventName => {
	dropArea.addEventListener(eventName, highlight, false)
})

;['dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, unhighlight, false)
})

// Handle dropped files
dropArea.addEventListener('drop', handleDrop, false)

function preventDefaults (e) {
	e.preventDefault()
	e.stopPropagation()
}

function highlight(e) {
	dropArea.classList.add('highlight')
}

function unhighlight(e) {
	dropArea.classList.remove('active')
}

function handleDrop(e) {
	var dt = e.dataTransfer
	var files = dt.files

	handleFiles(files)
}



function updateProgress(fileNumber, percent) {
	uploadProgress[fileNumber] = percent
	let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
	console.debug('update', fileNumber, percent, total)
}

function handleFiles(files) {
	files = [...files]
	files.forEach(uploadFile)
	files.forEach(previewFile)
}

function previewFile(file) {
	let reader = new FileReader()
	reader.readAsDataURL(file)
	reader.onloadend = function() {
		let img = document.createElement('img')
		img.src = reader.result
		document.getElementById('bm-gallery').innerHTML=`<img src='${reader.result}'/>`
	}
}

function uploadFile(file, i) {
	console.log('upload-file')
}


// changeTabs
let bmTabs=document.querySelectorAll('.profile-tab');
let tabContents=document.querySelectorAll('.profile-tab-content');
let removeActives=(val)=>{
	document.querySelectorAll(`.${val}`).forEach(el=>el.classList.remove('active'));
}
bmTabs.forEach((tab,i)=>{
	tab.addEventListener('click',()=>{
		console.log(i);
		removeActives('profile-tab');
		removeActives('profile-tab-content');
		bmTabs[i].classList.add('active');
		tabContents[i].classList.add('active');
	});
});


