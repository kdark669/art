<?php


require_once ('controller/ArtistInfo.php');
require_once ('controller/RegisterArtist.php');
require_once ('controller/ChangeAvatar.php');
require_once ('controller/UploadArt.php');

add_action( 'wp_head',                              array('ArtistInfo',     'add_login_Link'));
add_action( 'woocommerce_after_add_to_cart_button', array('ArtistInfo',     'our_artist_info' ));
add_filter('woocommerce_login_redirect',            array('RegisterArtist','wc_custom_user_redirect'),10, 2);



