<?php
require_once ('controller/artApprove.php');
require_once ('controller/CustomProduct.php');
add_filter( 'product_type_selector',            array('CustomProduct','wcpt_add_artist_type'));
add_filter( 'woocommerce_product_data_tabs',    array('CustomProduct','artist_tab'));
add_action( 'woocommerce_product_data_panels',  array('CustomProduct','wcpt_artist_options_product_tab_content'));
add_action( 'woocommerce_process_product_meta', array('CustomProduct','save_artist_options_field'));