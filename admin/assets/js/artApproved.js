function approve(e) {
    e.preventDefault();
    const post_id= document.getElementById('post_id').value;
    let url = window.location.href+'';
    let data = {
    };
    data =  new FormData();
    data.append('post_id',post_id);
    axios.post(url, data ,{
        params:{
            'custom':'approveArt'
        }
    }).then(res => {
        if(res.data.status){
            location.reload();
            var bmacceptbutton = document.getElementsById('bm-approved-button');
            var bmapproved = document.getElementsById('bm_approved');
            bmacceptbutton.style.display = 'none';
            bmapproved.style.display = 'flex';
            bmapproved.innerHTML = "Approved";
            console.log(bmapproved)
        }
    }).catch(error =>{

    })
}