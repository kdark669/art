<?php


if (!defined('ABSPATH')) {
    die();
}
if (!class_exists('CustomProduct')):
class CustomProduct
{
    public static function wcpt_add_artist_type ( $type ) {
        // Key should be exactly the same as in the class product_type
        $type[ 'artist' ] = __( 'Artist' );
        return $type;
    }

   public static function artist_tab( $tabs) {
        // Key should be exactly the same as in the class product_type
        $tabs['artist'] = array(
            'label'	 => __( 'Artist', 'wcpt' ),
            'target' => 'artist_options',
            'class'  => ('show_if_artist'),
        );
        return $tabs;
    }


    public static function wcpt_artist_options_product_tab_content(){
        global $wpdb;
        global $post;
        $value = update_post_meta( $post->ID, '_select', true );
        if(empty($value)){
            $value = '';
        }
        $options[''] = __( 'select a artist', 'woocommerce');
        $userTable = $wpdb->prefix .'users';
        $usermetaTable = $wpdb->prefix .'usermeta';
        $sql = "SELECT $userTable.ID, $userTable.user_nicename 
                FROM $userTable INNER JOIN $usermetaTable 
                ON $userTable.ID = $usermetaTable.user_id 
                WHERE $usermetaTable.meta_key = 'wp_capabilities' 
                AND $usermetaTable.meta_value LIKE '%artist%' 
                ORDER BY $userTable.user_nicename";
        $artist = $wpdb->get_results($sql);
        foreach ($artist as $key => $term)
            $options[$term->ID] = $term->user_nicename;
        $artistId = $wpdb->get_results("SELECT meta_value FROM $usermetaTable WHERE 'post_id' = $post->ID
        AND meta_key = '_selectartist'
        ");
        if(empty($artistId)){
            $artist_id = null;
        }else{
            $artist_id = $artistId[0]->meta_value;
        }

        ?><div id='artist_options' class='panel woocommerce_options_panel'><?php
            ?><div class='options_group'><?php
                woocommerce_wp_select( array(
                    'id'      => '_selectartist',
                    'label'   => __( 'Select Artist', 'wcpt' ),
                    'options' =>  $options, //this is where I am having trouble
                    'value'   => $artistId,
                ) );
            ?></div>
        </div><?php
    }

    public static function save_artist_options_field($post_id){
        $woocommerce_select = $_POST['_selectartist'];
        if(!empty( $woocommerce_select ) ) {
            update_post_meta($post_id, '_selectartist', $woocommerce_select);
        }
        else {
            update_post_meta( $post_id, '_selectartist',  '' );
        }
    }
}
endif;