<?php

if(!class_exists('CreatePost')):
class CreatePost
{
    public static function custom_post_product(){
        $labels = [
            'name'               => 'Arts',
            'singular'           =>'Arts',
            'add_new'            =>'Add Art',
            'all_items'          =>'All Arts',
            'add_new_item'       =>'Add Art',
            'edit_item'          => 'Edit Art',
            'new_item'           =>'New Art',
            'view_item'          =>'View Art',
            'search_item'        =>'Search Art  ',
            'not_found'          =>  'No Art found' ,
            'not_found_in_trash' =>  'No Art found in the Trash',
            'parent_item_colon'  =>  'Parent Item'
        ];
        $args  = [
            'labels' => $labels,
            'public'=> true,
            'has_archive'=> false,
            'publicly_queryable'=> true,
            'query_var'=>true,
            'rewrite' =>true,
            'capability_type' => 'post',
            'menu_position' => 5,
            'menu_icon' =>'dashicons-art',
            'supports' =>['title','description','editor', 'thumbnail', 'excerpt', 'comments'],
            'taxonomies' => [
                'category',
                'post_tag',
            ],
            'exclude_from_search'=> false
        ];
        register_post_type('art', $args);
//        print_r(register_post_type('art', $args));
//        die();
    }

    public static function set_custom_art_column($columns){
        unset( $columns['author'] );
        $columns['art_action'] = __( 'Action', 'Action' );
//        $columns['featured_image'] = __( 'Art Image', 'Image' );
        return $columns;
    }
    public static function custom_art_column($column, $post_id){
            switch ( $column ) {
                case 'art_action' :
                    $terms = get_the_term_list( $post_id , 'Action' , '' , ',' , '' );
                    if ( is_string( $terms ) ){
                       echo 'Accepted';
                    }
                    else {
                        $postmeta = get_post_meta($post_id);
                        $acceptedFlag = $postmeta['_acceptedFlag'][0];
                        if($acceptedFlag == 1){
                                echo '<span id="bm_approved" class="bm_approved">Accepted</span>';
                        }else{
                            wp_enqueue_style('bmapprove', plugins_url() . '/Art/admin/assets/css/style.css');
                            wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
                            wp_enqueue_script('bmapprove', plugins_url() . '/Art/admin/assets/js/artApproved.js', array(), false, true);
                            ?>
                            <form action="" method="post" class="bmapprove" id="bmapprove">
                                <input type="hidden" id="post_id" name="post_id" value="<?php echo $post_id ?>">
                                <button type="submit" id="bm-approved-button" class="bm-approved-button" style="cursor: pointer;" onclick="approve(event)">Accept</button>
                            </form>
                            <span id="bm_approved" class="bm_approved"></span>
                            <?php
                        }

                    }
                    break;

                case 'featured_image' :
                    $postmeta = get_post($post_id)->post_content;
                    ?>
<!--                    <form action="" >-->
<!--                        <div class="" style="width: 3%;">-->
<!--                            --><?php //echo $postmeta?>
<!--                        </div>-->
<!--                    </form>-->
                    <?php
                    break;
            }
    }

}
endif;