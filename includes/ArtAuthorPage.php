<?php

if (!defined('ABSPATH')) {
die;
}
if (!class_exists('ArtAuthorPage')):
class ArtAuthorPage
{
    public static function createRole()
    {
        add_role(
            'artist',
            'Artist',
            array(
                'read' => true,
                'delete_posts' => false
            )
        );
    }
    public static function wpa_155871_template_loader( $template ) {
        if(get_query_var( 'author_name' )){
            $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
        }
        if(!empty($author) || isset($_GET['author'])){
            set_query_var( 'author_id', isset($_GET['author']) ? $_GET['author']: $author->ID );
            if(isset($_GET['author']) || isset($author->ID)){
                $user_id = isset($_GET['author']) ? $_GET['author']: $author->ID;
                if(empty($user_id)){
                    $finds[] = WP_PLUGIN_DIR . '/Art/templates/registerArtist.php';
                    $template = locate_template(array_unique($finds));
                    if (!$template) {
                        wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
                        wp_enqueue_style('form', plugins_url() . '/Art/public/assets/css/style.css');
                        wp_enqueue_script('form', plugins_url() . '/Art/public/assets/js/RegisterArt.js', array(), false, true);
                        $template =  WP_PLUGIN_DIR . '/Art/templates/registerArtist.php';
                    }
                }
                else{
                    $user_meta=get_userdata($user_id);
                    $file = '';
                    $plugin_dir = ABSPATH . 'wp-content/plugins/Art/';
                    if(empty($user_meta->roles[0])){
                        global $wp_query;
                        $wp_query->set_404();
                        status_header( 404 );
                        get_template_part( 404 ); exit();
                    }
                    $user_roles=$user_meta->roles[0];
                    switch ($user_roles) {
                        case "administrator":
                            echo '';
                            break;
                        case "artist":
                            if (is_author()) {
                                $file = 'artistProfile.php'; // the name of your custom template
                                $find[] = $file;
                                $find[] = 'Art/templates/' . $file;
                            }
                            if ($file) {
                                $template = locate_template(array_unique($find));
                                if (!$template) {
                                    // if not found in theme, will use your plugin version
                                    wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
                                    wp_enqueue_style('wrapper', plugins_url() . '/Art/public/assets/css/bmlayout.css');
                                    wp_enqueue_style('wrapper1', plugins_url() . '/Art/public/assets/css/bmtheme.css');
                                    wp_enqueue_style('wrapper2', plugins_url() . '/Art/public/assets/css/bmvars.css');
                                    wp_enqueue_style('container-fluid', plugins_url() . '/Art/public/assets/css/artistStyle.css');
                                    wp_enqueue_script('wrapper-script', plugins_url() . '/Art/public/assets/js/bmmain.js', array(), false, true);
                                    wp_enqueue_script('uploadForm', plugins_url() . '/Art/public/assets/js/artUpload.js', array(), false, true);
                                    wp_enqueue_script('userAvatar', plugins_url() . '/Art/public/assets/js/userAvatar.js', array(), false, true);
                                    $template = untrailingslashit($plugin_dir) . '/templates/' . $file;
                                }
                            }
                            break;
                    }
                }
                return $template;
            }
        }
            else{
                global $wp;
                echo home_url( $wp->request );
                return $template;
            }
        }
}
endif;