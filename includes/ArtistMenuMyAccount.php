<?php

if(!defined('ABSPATH')){
    die();
}
if(!class_exists('ArtistMenuMyAccount')):
class ArtistMenuMyAccount
{
   public static function my_account_new_endpoints() {
        add_rewrite_endpoint( 'artist', EP_ROOT | EP_PAGES );
    }
    public static function artist_endpoint_content() {
        $user = wp_get_current_user();
        $user_roles=$user->roles[0];
        $user_id = $user->ID;
        if($user_roles == 'artist') {
            ?>
            <a href="<?php echo site_url() .'?author='.$user_id?>">
                Vist Profile
            </a>
            <?php
        }
        else {
            ?>
            <div class="onlyAdmin">
                <p>Would You Like To Be An Artist</p>
                <form action="" method="post">
                    <button type='submit' name="beArtist" class="btn"> Yes</button>
                    <?php
                    if (isset($_POST['beArtist'])) {
                        $u = new WP_User($user->ID);
                        $u->remove_role('customer');
                        $u->add_role('artist');
                        echo "<script type='text/javascript'>
                        window.location=document.location.href;
                        </script>";
                    }
                    ?>
                </form>
            </div>
            <?php
        }
    }
    public static function my_account_menu_order() {
        $menuOrder = array(
            'dashboard'          => __( 'Dashboard', 'woocommerce' ),
            'artist'             => __( 'Artist', 'woocommerce' ),
            'orders'             => __( 'Your Orders', 'woocommerce' ),
            'downloads'          => __( 'Download', 'woocommerce' ),
            'edit-address'       => __( 'Addresses', 'woocommerce' ),
            'edit-account'    	=> __( 'Account Details', 'woocommerce' ),
            'customer-logout'    => __( 'Logout', 'woocommerce' ),

        );
        return $menuOrder;
    }


    public static function wc_extra_registation_fields() {
       ?>
         <p class="form-row form-row-wide">
             <?php
             if(isset($_GET['new'])){
                 echo " You will register as Artist";
             }else{
                 echo "You will register as Customer";
             }
             ?>
            <input type="hidden" class="input-text" name="role" id="reg_role" value="<?php
                if(isset($_GET['new'])){
                    echo $_GET['new'];
                }else{
                    echo "customer";
                }
                ?>"/>
         </p>
        <?php
    }

    public static function wc_save_registration_form_fields( $customer_id ) {
        if ( isset($_POST['role']) ) {
            $role = $_POST['role'];
            $user = new WP_User($customer_id);
            if( $role == 'artist'){
                $user->set_role('artist');
            }
            else{
                $user->set_role('customer');
            }
        }
    }
}
endif;