<?php

if(!defined('ABSPATH')){
    die();
}
if(!class_exists('Database')):
    class Database
    {
        public function getTableName()
        {
            global $wpdb;
            return $wpdb->prefix . 'art_itg';
        }
        public static function createDatabaseTable() {
            global $wpdb;
            $table_name = self::getTableName();
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name(
				id int(11) NOT NULL AUTO_INCREMENT,
				product_id bigint(11) unsigned NOT NULL,
				post_id bigint(11) unsigned NOT NULL,
				PRIMARY KEY  (id),
				FOREIGN KEY (product_id) REFERENCES wp_posts(`ID`)
				) $charset_collate";

            $sql2 = "CREATE TABLE wp_artistprofile_itg(
				id int(11) NOT NULL AUTO_INCREMENT,
				user_id bigint(11) unsigned NOT NULL UNIQUE,
				profile varchar(100) NOT NULL,
                created datetime NOT NULL,
				PRIMARY KEY  (id),
				FOREIGN KEY (user_id) REFERENCES wp_users(`id`)
				) $charset_collate";
            $wpdb->query($sql2);
            $wpdb->query($sql);
        }

        public function removeArtistDatabase(){
            global $wpdb;
            $table_name  = self::getTableName();
            $sql = "DROP TABLE IF EXISTS $table_name";
            $sql2 = "DROP TABLE IF EXISTS wp_artistprofile_itg";
            $wpdb->query($sql2);
            $wpdb->query($sql);
        }

    }
endif;