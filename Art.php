<?php

/**
 * Plugin Name: Art
 * Description: Plugin That Artist can upload the Art.
 * Version: 1.0.0
 * Author: ITGlance
 * Author URI:
 * Text Domain: itglance.org
 * Domain Path: languages
 *
 * License: GPLv2 or later
 * Domain Path: languages
 *
 */

if (!defined('ABSPATH')) {
    die('We\'re sorry, but you can not directly access this file.');
}

if (!class_exists('Art')):
class Art
{
    private static $instance = null;

    private function __construct() {
        $this->initializeHooks();
        $this->setupDatabase();
        $this->removeDatabase();
        $this->createpost();
        $this->createArtistMenu();
        $this->createRoles();
    }

    public static function getInstance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function initializeHooks() {
        if ( is_admin() ) {
            require_once( 'admin/admin.php' );
        }
        require_once( 'public/public.php' );
    }

    private function setupDatabase() {
        require_once ('includes/Database.php');
        register_activation_hook( __FILE__,              array('Database','createDatabaseTable' ) );
    }
    private function removeDatabase() {
        require_once ('includes/Database.php');
        register_deactivation_hook( __FILE__,           array('Database','removeArtistDatabase' ) );
    }

    private function createRoles(){
        require_once ('includes/Role.php');
        register_activation_hook( __FILE__,             array('Role','createRole' ) );
    }
    public function createpost(){
        require_once ('admin/controller/CreatePost.php');
        add_action( 'init',                                 array('CreatePost','custom_post_product'));
        add_action( 'manage_art_posts_custom_column' ,      array('CreatePost','custom_art_column'), 10, 2 );
        add_filter( 'manage_art_posts_columns',             array('CreatePost','set_custom_art_column') );
    }

    private function createArtistMenu(){
        require_once('includes/ArtAuthorPage.php');
        require_once ('includes/ArtistMenuMyAccount.php');
        add_filter( 'template_include',                     array('ArtAuthorPage',  'wpa_155871_template_loader' ));
        add_action( 'init',                                 array('ArtistMenuMyAccount','my_account_new_endpoints'));
        add_action( 'woocommerce_account_artist_endpoint',  array('ArtistMenuMyAccount','artist_endpoint_content' ));
        add_filter ( 'woocommerce_account_menu_items',      array('ArtistMenuMyAccount','my_account_menu_order'));
        add_action( 'woocommerce_register_form',            array('ArtistMenuMyAccount','wc_extra_registation_fields'));
        add_action( 'woocommerce_created_customer',         array('ArtistMenuMyAccount','wc_save_registration_form_fields' ));
    }
}
Art::getInstance();
endif;