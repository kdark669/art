
<?php

get_header();
global $wpdb;
global $wp_query;
$user_id = get_current_user_id();
$postTable = $wpdb->prefix .'posts';
$authorTable = $wpdb->prefix .'artistprofile_itg';
$author_id =  isset($_GET['author']) ? $_GET['author'] : get_query_var('author');
$author_meta=get_userdata($author_id);
$author_roles=$author_meta->roles[0];
$author_avatar_url = get_avatar_url($author_id);
$author_avatar='';
$user_avatar = $wpdb->get_row($wpdb->prepare(" SELECT id,profile FROM $authorTable where user_id = $author_id"));
//$author_post = $wpdb->get_results(" SELECT * FROM $postTable where post_author = $author_id
//                                    and post_type = 'art' and post_status = 'publish' ");
//$author_post = $wpdb->get_results(" SELECT * FROM $postTable where post_author = $author_id
//                                    and post_type = 'product' and post_status = 'publish' ");
//die();
$args = array(
        'post_type'      => 'art',
        'author'         => $current_user->ID,
        'post_status' => 'any',
//        'posts_per_page' => 1
    );
$author_post = get_posts( $args );
$args = array( 'post_type' => 'product', 'product_cat' => 'Uncategorized', 'orderby' => 'rand' );
    $category = new WP_Query( $args );
    $products = $category->posts;
if(!empty($user_avatar)){
    $author_avatar = $user_avatar;
}

?>
<main class="wrapper">
		<section class="profile">
    <?php
     $current_user_id = get_current_user_id();
    if(is_user_logged_in()){
        if( $current_user_id == $author_id){
            show_profile_card($author_avatar,$author_avatar_url, $author_meta,$author_id);
//            show_art_upload_form();
//            show_collection($author_post,$current_user_id,$author_id);
            displayCollectionOrArtUpload($author_post,$current_user_id,$author_id,true);
        }
        else{
            show_profile_card($author_avatar, $author_avatar_url, $author_meta,$author_id);
//            show_collection($author_post,$current_user_id,$author_id);
            displayCollectionOrArtUpload($author_post,$current_user_id,$author_id,false);
        }
    }
    else{
        show_profile_card($author_avatar, $author_avatar_url, $author_meta,$author_id);
//        show_collection($author_post, $current_user_id, $author_id);
displayCollectionOrArtUpload($author_post,$current_user_id,$author_id,false);
    }
    ?>
</section>
</main>
<div class="image-light-box">
		<div class="backdrop" onclick="displayImage()"></div>
		<img class="light-box-image" src="">
		<h2 class="uppercase light-text light-box-title"></h2>
	</div>
	<div id="uploadBox" class="d-none">
		<div class="background" onclick="displayUploadBox(false)"></div>
		<form class="upload-card" id="uploadArt" method="post" enctype="multipart/form-data">
		<input type="hidden" name="artistId" id="artistId" value="<?php echo $user_id; ?>">
			<h1>Upload</h1>
				<div class="bm-form-group">
			<label class="uppercase bold title">Title</label>
			<div class="bm-input">
				<input type="text" name="title" placeholder="Enter the title">
<!--				<div class="bm-icon"><img src="https://cdn1.iconfinder.com/data/icons/internet-28/48/57-512.png"></div>-->
			</div>
		</div>
		<div class="mt-5 d-flex flex-column w-100">
			<label class="uppercase bold title">Select Categories to apply on</label>
			<div class="my-5 bm-select d-flex justify-evenly flex-wrap">
				<?php foreach($products as $p) {
                                echo '<label class="bm-container" for="cat'.$p->ID.'">'.$p->post_title.'
							  <input type="checkbox" id="cat'.$p->ID.'" name="cat" value="'.$p->ID.'">
							  <span class="bm-checkmark"></span>
							</label>
                            ';
                            }?>
			</div>
		</div>
		<div id="drop-area">
		  <div class="bm-form">
		  		<div class="w-100 flex-centered">
					<label class="bold uppercase">Drag to upload or</label>
					<div class="bm-form-group">
		    			<label class="btn primary text-center" for="fileElem">Click to upload art</label>
						 <input type="file" id="fileElem"  name="artistImage" placeholder="Artist Image"onchange="handleFiles(this.files)">
					</div>
				</div>
		  </div>
			<div id="bm-gallery" /></div>
		</div>
		<button class="upload-btn btn primary">Upload</button>
		</form>
	</div>
<?php
function displayCollectionOrArtUpload($author_post,$current_user_id,$author_id,$displayForm){

    ?>
    <div class="profile-content">
				<div class="profile-tabs">
					<div class="profile-tab active">Collections</div>
<!--					<div class="profile-tab">Upload Art</div>-->
				</div>
				<div class="profile-tab-content active profile-content-area">
				<?php
                       if (empty($author_post)) {
                           ?>
                           <p>Collection is Empty</p>
                           <?php
                       }
                       foreach ($author_post as $a) {
//                        ?>
                        <div class="collection-image-box">
                            <div class="image-actions">
                                <?php
                                    if($current_user_id == $author_id){
                                        echo ' <div class="image-action edit-image" onclick=\'changeFormToEdit(this,`<?php echo $a->ID;?>`,`<?php echo $a->post_title;?>`,`<?php echo $a->post_content;?>`)\'><i class="material-icons">edit</i></div>';
                                    }
                                ?>
                                <div class="image-action view-image" onclick="displayImage(this,'flex')"><i class="material-icons">visibility</i></div>
						     </div>
                           <?php echo "$a->post_content";?>
                           </div>
                           <?php
                       }
                       ?>

				</div>
<!--				</div>-->
		</div>

<?php
}

function show_profile_card($avatar ,$avatar_url, $user_data,$author_id){
    $current_user_id = get_current_user_id();
    $pluginurl = plugins_url('Art');
    ?>
<!--    <div class="profile-card">-->
<!--        <div class="banner">-->
<!--            <img src="--><?php //echo $pluginurl . '/public/assets/img/banner.png' ?><!--" class="banner-img">-->
<!--        </div>-->
<!--        <div class="updated notice success" id="uploaded">-->
<!--        </div>-->
<!--        <div class="errorR notice" id="errorR">-->
<!--        </div>-->
<!--        <div class="profile-info">-->
<!--            <div class="profile-pic">-->
<!--                --><?php //if (empty($avatar)) {
//                    ?>
<!--                    <img src="--><?php //echo $avatar_url ?><!--">-->
<!--                    --><?php
//                } else { ?>
<!--                    <img src="--><?php //echo $pluginurl . '/Profiles/' . $avatar ?><!--">-->
<!--                --><?php //} ?>
<!--                --><?php //if($current_user_id == $author_id){?>
<!--                    <form method="post" enctype="multipart/form-data" class="userAvatar" id="userAvatar">-->
<!--                        <input type="hidden" name="user_id" id="user_id" value="--><?php //echo get_current_user_id(); ?><!--">-->
<!--                        <span class="error" id="euser_avatar" style="color: #e25454"></span>-->
<!--                        <input type="hidden" name="a_id" id="a_id" value="--><?php //echo $avatar->id ?><!--">-->
<!--                        <input type="file" name="user_avatar" id="user_avatar">-->
<!--                        <button type="submit"> Change Avatar</button>-->
<!--                    </form>-->
<!--                --><?php //} ?>
<!--            </div>-->
<!--            <div class="artist-info">-->
<!--                <p class="artist-name">--><?php //echo $user_data->display_name ?><!--</p>-->
<!--                <p class="artist-bio">ARTIST, ENTREPRENEUR, DREAMER</p>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
        <div class="profile-header">
            <?php
                if($current_user_id == $author_id){
                    echo '
                        <div class="change-cover">
                            <button onclick="changeImage(\'changeCover\')" class="btn primary">Change Cover</button>
                            <input class="d-none" id="changeCover" type="file"/>
                        </div>
                    ';
                }
            ?>

			<img src="<?php echo $pluginurl . '/public/assets/img/banner.png' ?>">
			<div class="profile-details">
				<div class="profile-picture">
<!--					<img class="image-cover" src="../assets/images/image.png">-->
                    <?php if (empty($avatar)) {
                    ?>
                    <img class="image-cover" src="<?php echo $avatar_url ?>">
                    <?php
                } else { ?>
                    <img class="image-cover" src="<?php echo $pluginurl . '/Profiles/' . $avatar->profile ?>">
                <?php } ?>
					<form class="change-profile" id="userAvatar" onclick="changeImage('changeProfile')">
					 <input type="hidden" name="user_id" id="user_id" value="<?php echo get_current_user_id(); ?>">
                        <input type="hidden" name="a_id" id="a_id" value="<?php echo $avatar->id ?>">
						<img src="https://cdn1.iconfinder.com/data/icons/internet-28/48/57-512.png">
						<input type="file" id="changeProfile" class="d-none" name="user_avatar" onchange="onAvatarChange(event)">
					</form>
				</div>
				<div class="profile-primary-details-area">
					<div class="profile-primary-details">
						<div class="profile-name title uppercase"><?php echo $user_data->display_name ?></div>
<!--						<div class="profile-tag uppercase bold"> Artist, Entreprenuer, Dreamer</div>-->
					</div>
					<?php
					    if($current_user_id == $author_id){
					        echo '
					        <button class="profile-arts btn primary flex-centered" onclick="displayUploadBox()">
                                Upload
                            </button>
					        ';
					    }
					?>
				</div>
			</div>
		</div>
    <?php
}
function show_collection($posts,$current_user_id,$author_id){
    ?>
    <div class="artist-collection">
                   <p class="artist-collection-title">YOUR COLLECTIONS</p>
                   <div class="artist-collection-items">
                       <?php
                       if (empty($posts)) {
                           ?>
                           <p>Collection is Empty</p>
                           <?php
                       }
                       foreach ($posts as $a) {
//                        ?>
                           <div class="card">
                               <div class="img-container">
                                   <?php echo "$a->post_content";?>
                               </div>
                               <div class="container">
                                   <h4><b><?php echo $a->post_title;?></b></h4>
                               </div>
                               <form id="<?php echo $a->ID;?>">
                                   <input type="hidden" name="">
                               </form>

                               <?php if ($current_user_id == $author_id){?>
                               <div class="backdrop">
                                   <button onclick='changeFormToEdit(`<?php echo $a->ID;?>`,`<?php echo $a->post_title;?>`,`<?php echo $a->post_content;?>`)' id="<?php echo $a->ID;?>">edit</button>
                                    <button >view</button>
                               </div>
                               <?php }else{ ?>
                                <div class="backdrop">
                                    <button >view</button>
                               </div>
                               <?php } ?>
                           </div>
                           <?php
                       }
                       ?>
                   </div>


    <?php
}
function show_art_upload_form(){
    $args = array( 'post_type' => 'product', 'product_cat' => 'Uncategorized', 'orderby' => 'rand' );
    $category = new WP_Query( $args );
    $products = $category->posts;
    ?>
    <div class="Art">
        <form class="uploadArt" id="uploadArt" method="post" enctype="multipart/form-data">
            <input type="text" name="title" placeholder="Enter the title">
            <label for="artistId">Upload Arts</label>
            <input type="hidden" name="artistId" id="artistId" value="<?php echo get_current_user_id(); ?>">
            <div class="form-group">
                <img id="blah" src="https://acadianakarate.com/wp-content/uploads/2017/04/default-image.jpg"
                     alt="your image"/>
                <input type="file" name="artistImage" id="artistImage" placeholder="Artist Image"
                       onblur="validateImage(event)">
                <span class="error" id="eartist_file"></span>
            </div>
            <div class="form-item">
                <div class="item-header">Select Products to Apply Art</div>
                <?php foreach($products as $p) {
                    echo '<input type="checkbox" id="cat'.$p->ID.'" name="cat" value="'.$p->ID.'">
                                <label for="cat'.$p->ID.'">'.$p->post_title.'</label>
';
                }

                ?></div>
            <!--                </select>-->
            <button type="submit">Upload Art</button>
        </form>
    </div>

    <?php
}

get_footer();
?>